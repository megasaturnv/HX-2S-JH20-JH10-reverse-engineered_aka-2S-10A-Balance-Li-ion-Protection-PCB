# HX-2S-JH20 reverse-engineered (2S-10A-Balance-Li-ion-Protection-PCB)

## Circuit trace:
![HX-2S-JH20 - Circuit trace](docs/HX-2S-JH20_Circuit-trace.jpg)

## Components:
![HX-2S-JH20 - Components](docs/HX-2S-JH20_Components.jpg)

## Kicad Eeschema Schematic
### Work in Progress
[HX-2S-JH20 PDF](docs/HX_2S_JH20.pdf)
